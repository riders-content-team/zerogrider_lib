import rospy
from zerogrider_description.srv import CameraService
from zerogrider_description.srv import LaserService
from geometry_msgs.msg import Vector3
from std_msgs.msg import Bool
from math import pi
import time

class RobotControl:

    def __init__(self,topic_name,node_name):
        self.topic_name = topic_name
        self.node_name = node_name

        rospy.init_node(node_name, anonymous=True)
        self.pub = rospy.Publisher(topic_name,Vector3,queue_size=1)
        self.vel_msg = Vector3()

        self.dock_sub = rospy.Subscriber("dock",Bool,self.dockingHandler)
        self.dock_state = False
        self.contact_sub = rospy.Subscriber("contact",Bool,self.contactHandler)
        self.contact_state = False

        self.image = self._Image()
        self.init_sensor_services()
        self.last_errX = 0
        self.forceR = 50
        self.forceL = 50
        self.velocityY = 0
        self.last_offsetY = 0
        self.last_time = 0

    def init_sensor_services(self):
        try:
            self.image_data_service = rospy.ServiceProxy('/zerogrider/get_image', CameraService)
            self.front_laser_service = rospy.ServiceProxy('/zerogrider/get_front_range',  LaserService)
            resp = self.image_data_service()

            self.image.height = resp.height
            self.image.width = resp.width

            return True

        except (rospy.ServiceException, rospy.ROSException):
            print("no camera sensor")
            self.status = "stop"
            return False

    def front_laser_data(self):
        resp = self.front_laser_service()
        return resp.data


    def image_data(self):
        resp = self.image_data_service()

        #self.image.data = resp.data
        # create 3D image array. 
        rows,cols = (self.image.height, self.image.width)
        self.image.data = [[[0 for i in range(3)] for j in range(cols)]for k in range(rows)] 
        l = 0
        for i in range(self.image.height):
            for j in range(self.image.width):
                for k in range(3):
                    self.image.data[i][j][k] = ord(resp.data[l])
                    l = l + 1
        #print(self.image.data)
        return self.image

    # check the docking point is in the middle of the screen
    def centerPointController(self):
        middle_x = self.image.width/2
        middle_y = self.image.height/2
        last_errY = 0
        Kp = 0.001
        Kd = 0.001
        break_stat = False # break statement for break double loop. if it find green pixel position, it doesn't need to continue
        for i in range(self.image.width):
            for j in range(self.image.height):
                if(self.image.data[i][j][1] >= 245 and i == middle_x and j == middle_y):
                    print("go straigth")
                    self.forward(100)
                    break_stat = True
                    break
                elif(self.image.data[i][j][1] >= 245):
                    
                    ################################## test-1 / checkpoint 1
                    # offsetY = j - middle_y
                    # print(offsetY)
                    # if(offsetY>0):
                    #     self.right(100)
                    # else:
                    #     self.left(100)
                    ###################################checkpoint 2
                    # offsetY = j - middle_y
                    # cur_time = time.time()
                    # deltaT = cur_time - self.last_time
                    # #print("deltaT ",str(deltaT))
                    # self.velocityY = -(offsetY - self.last_offsetY)/deltaT
                    # print(offsetY - self.last_offsetY)
                    # #print("velocity: ",str(self.velocityY))
                    # if(self.velocityY>0):
                    #     self.left(100)
                    # else:
                    #     self.right(100)
                    # self.last_time = cur_time
                    # self.last_offsetY = offsetY
                    #################################### test-2 pid
                    # error = middle_y - j
                    # U = Kp*error + Kd*(error-self.last_errX)
                    # print(U)
                    # self.last_errX = error
                    # self.forceR = self.forceR - U 
                    # self.forceL = self.forceL + U
                    # print("right: ", str(self.forceR))

                    # self.right(self.forceR)
                    # self.left(self.forceL)
                    # if(offsetY != 0):  #offsetY for test-1 and for test-2 write error instead of offsetY
                    #     break_stat = True
                    #     break
                    ################################### test-3
                    # this test works for x=0 and z =0 initial position
                    force = 100
                    if(i > middle_x and j > middle_y):
                            print("turn right and go down")
                            self.down(force)
                            self.right(force)
                            break_stat = True
                            break
                    elif(i > middle_x and j < middle_y):
                            print("turn left and go down")
                            self.left(force)
                            self.down(force)
                            break_stat = True
                            break
                    elif(i < middle_x and j > middle_y):
                            print("turn right and go up")
                            self.right(force)
                            self.up(force)
                            break_stat = True
                            break
                    elif(i < middle_x and j < middle_y):
                            print("turn left and go up")
                            self.up(force)
                            self.left(force)
                            break_stat = True
                            break
                elif(i==self.image.height-1 and j ==self.image.width-1):
                    print("the point is not visible turn around")
            if(break_stat):
                break

    def contactHandler(self,contact):
        if contact.data:
            self.contact_state = True

    def dockingHandler(self,dock):
        if dock.data:
            self.dock_state = True


    def up(self,force):
        #todo
        force = force*2*pi/360
        self.vel_msg.x = 0
        self.vel_msg.y = 0
        self.vel_msg.z = force
        self.pub.publish(self.vel_msg)

    def down(self,force):
        force = force*2*pi/360
        self.vel_msg.x = 0
        self.vel_msg.y = 0
        self.vel_msg.z = -force
        self.pub.publish(self.vel_msg)
        
    def left(self, force):
        force = force*2*pi/360
        self.vel_msg.x = -force
        self.vel_msg.y = 0
        self.vel_msg.z = 0
        self.pub.publish(self.vel_msg)

    def right(self, force):
        force = force*2*pi/360
        self.vel_msg.x = force
        self.vel_msg.y = 0
        self.vel_msg.z = 0
        self.pub.publish(self.vel_msg)

    def forward(self, force):
        force = force*2*pi/360
        self.vel_msg.x = 0
        self.vel_msg.y = force
        self.vel_msg.z = 0
        self.pub.publish(self.vel_msg)

    def backward(self, force):
        force = force*2*pi/360
        self.vel_msg.x = 0
        self.vel_msg.y = -force
        self.vel_msg.z = 0
        self.pub.publish(self.vel_msg)

    def is_ok(self): 
        if not rospy.is_shutdown() and not self.dock_state and not self.contact_state:
            return True         
        else:         
            return False

    class _Image:
        def __init__(self):
            self.height = 0
            self.width = 0
            self.data = []
