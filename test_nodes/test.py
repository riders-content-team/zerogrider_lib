#!/usr/bin/env python2
from zerogrider_lib.robot_control import RobotControl
import rospy



node_name = 'test_node'
topic_name = '/robot_force'
robot = RobotControl(topic_name,node_name)

image = robot._Image()
#robot.right(1000)
while robot.is_ok():
    image = robot.image_data()
    robot.centerPointController()